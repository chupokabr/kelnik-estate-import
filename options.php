<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'kelnik.estateimport');

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Text\HtmlConverter;

if (!$USER->isAdmin()) {
    $APPLICATION->authForm('Nope');
}

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();
Loc::loadMessages($context->getServer()->getDocumentRoot()."/bitrix/modules/main/options.php");
Loc::loadMessages(__FILE__);

$tabControl = new CAdminTabControl("tabControl", array(
    array(
        "DIV" => "common",
        "TAB" => Loc::getMessage("KELNIK_IMPORT_TAB_COMMON_NAME"),
        "TITLE" => Loc::getMessage("KELNIK_IMPORT_TAB_COMMON_TITLE"),
    ),
    array(
        "DIV" => "xml",
        "TAB" => Loc::getMessage("KELNIK_IMPORT_TAB_XML_NAME"),
        "TITLE" => Loc::getMessage("KELNIK_IMPORT_TAB_XML_TITLE"),
    ),
));

if (!empty($save) && $request->isPost() && check_bitrix_sessid()) {
    $saveFields = [
        'email_import_error',
        'log_rotate',
        'xml_msg_num',
        'xml_msg_sent'
    ];

    if (!empty($_POST)) {
        foreach($saveFields AS $type => $field) {
            Option::set(
                ADMIN_MODULE_NAME,
                $field,
                $request->getPost($field)
            );
        }

        CAdminMessage::showMessage(array(
            "MESSAGE" => Loc::getMessage("KELNIK_IMPORT_REFERENCES_OPTIONS_SAVED"),
            "TYPE" => "OK",
        ));
    } else {
        CAdminMessage::showMessage(Loc::getMessage("KELNIK_IMPORT_REFERENCES_INVALID_VALUE"));
    }
}
$tabControl->begin();
?>

<form method="post" action="<?=sprintf('%s?mid=%s&mid_menu=%s&lang=%s', $request->getRequestedPage(), urlencode($mid), urlencode($mid_menu), LANGUAGE_ID)?>">
    <?php
    echo bitrix_sessid_post();
    $tabControl->beginNextTab();
    ?>
    <tr>
        <td width="40%">
            <label for="email_import_error"><?=Loc::getMessage("KELNIK_IMPORT_EMAIL_IMPORT_ERROR") ?>:</label>
        </td>
        <td width="60%" colspan="2">
            <input type="text"
                   size="50"
                   maxlength="255"
                   name="email_import_error"
                   value="<?=HtmlConverter::getHtmlConverter()->encode(Option::get(ADMIN_MODULE_NAME, "email_import_error", ''));?>"
            />
        </td>
    </tr>
    <tr>
        <td width="40%">
            <label for="log_rotate"><?=Loc::getMessage("KELNIK_IMPORT_LOG_ROTATE") ?>:</label>
        </td>
        <td width="40%">
            <input type="text"
                   size="50"
                   maxlength="255"
                   name="log_rotate"
                   value="<?=HtmlConverter::getHtmlConverter()->encode(Option::get(ADMIN_MODULE_NAME, "log_rotate", 20));?>"
            />
        </td>
        <td width="20%" style="font-size: smaller; color: #666; padding-left: 10px;">
            <?= Loc::getMessage('KELNIK_IMPORT_LOG_ROTATE_LIMIT'); ?>
        </td>
    </tr>

    <? $tabControl->beginNextTab(); ?>
    <tr>
        <td width="40%">
            <label for="xml_msg_num"><?=Loc::getMessage("KELNIK_IMPORT_XML_MSG_NUM") ?>:</label>
        </td>
        <td width="60%" colspan="2">
            <input type="text"
                   size="50"
                   maxlength="255"
                   name="xml_msg_num"
                   value="<?=HtmlConverter::getHtmlConverter()->encode(Option::get(ADMIN_MODULE_NAME, "xml_msg_num", 0));?>"
            />
        </td>
    </tr>
    <tr>
        <td width="40%">
            <label for="xml_msg_sent"><?=Loc::getMessage("KELNIK_IMPORT_XML_MSG_SENT") ?>:</label>
        </td>
        <td width="60%" colspan="2">
            <input type="text"
                   size="50"
                   maxlength="255"
                   name="xml_msg_sent"
                   value="<?=HtmlConverter::getHtmlConverter()->encode(Option::get(ADMIN_MODULE_NAME, "xml_msg_sent", 0));?>"
            />
        </td>
    </tr>


    <?php
    $tabControl->buttons();
    ?>
    <input type="submit"
           name="save"
           value="<?=Loc::getMessage("MAIN_SAVE") ?>"
           title="<?=Loc::getMessage("MAIN_OPT_SAVE_TITLE") ?>"
           class="adm-btn-save"
    />
    <?php
    $tabControl->end();
    ?>
</form>