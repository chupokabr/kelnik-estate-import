<?php

namespace Kelnik\EstateImport\Parser;

use Monolog\Logger;

abstract class ParserAbstract implements ParserInterface
{
    protected $_workDir;

    public $logger;

    public function __construct($dir, Logger $logger)
    {
        $this->_workDir = $dir;
        $this->logger = $logger;
    }

    public function addValuesFromArray(array $row, $field)
    {
        if (!$field || !$row) {
            return $row;
        }

        foreach($row[$field] as $k => $v) {
            if (!is_array($v)) {
                $row[$field . '_' . strtoupper($k)] = $v;
                continue;
            }

            foreach ($v as $ki => $vi) {
                $row[$field . '_' . strtoupper($k) . '_' . strtoupper($ki)] = $vi;
            }
        }

        return $row;
    }

    public function getRowData(array $fields, $obj)
    {
        if (!$fields || !$obj) {
            return false;
        }

        if (is_object($obj)) {
            $obj = json_decode(
                json_encode($obj),
                true
            );
        }

        $row = [];

        foreach($fields as $key => $field) {

            if (!empty($field['type']) && $field['type'] === 'int') {
                $row[$key] = (int) $obj[$field['name']];
                continue;
            }

            if (!empty($field['type']) && $field['type'] === 'float') {
                $row[$key] = (float) $obj[$field['name']];
                continue;
            }

            if (!empty($field['type']) && $field['type'] === 'array') {
                $row[$key] = $obj[$field['name']];
                continue;
            }

            $row[$key] = trim($obj[$field['name']]);
        }

        return $row;
    }
}