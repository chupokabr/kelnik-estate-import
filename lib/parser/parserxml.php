<?php

namespace Kelnik\EstateImport\Parser;

use Kelnik\EstateImport\Import\Import;

class ParserXML extends ParserAbstract
{
    const IMPORT_FILE = 'Message_UPN_Site.xml';
    const EXPORT_FILE = 'Message_Site_UPN.xml';

    protected $_xmlRoot       = NULL;
    protected $_xmlBody       = NULL;
    protected $_xmlNamespace  = 'v8msg';
    protected $_messageNum    = 0;
    protected $_receivedNum   = 0;
    protected $_allowMethods  = [
        'ReferenceBook',
        'Objects',
        'Buildings',
        'Flats',
        'Premises'
    ];

    protected $_data = [];

    public function getData()
    {
        if (!$this->getXml()) {
            $this->logger->warning('no XML data');
            return false;
        }

        $this->logger->info('parse XML');

        foreach($this->_allowMethods as $v) {
            $methodName = 'process' . $v;
            if (!isset($this->_xmlBody->$v) || !method_exists($this, $methodName)) {
                continue;
            }

            $this->logger->info('= process "' . $v . '"');
            $this->$methodName($this->_xmlBody->$v);
            unset($this->_xmlBody->$v);
            $this->logger->info('= process "' . $v . '" complete');
        }

        $this->logger->info('parse complete');

        $this->setAnswer();

        return $this->_data;
    }

    public function getXml()
    {
        $this->logger->info('read ' . self::IMPORT_FILE);

        $importFile = $this->_workDir . DIRECTORY_SEPARATOR . self::IMPORT_FILE;

        if (!file_exists($importFile)) {
            $this->logger->warning('File "' . self::IMPORT_FILE . '" not exists');
            return false;
        }

        try {
            $xml = simplexml_load_file($importFile);
        } catch (Exception $e) {
            $this->logger->error('Can\'t load XML file: ', $e->getMessage());
            return false;
        }

        $ns = $xml->getNamespaces(true);

        $this->_xmlRoot = $xml->children($ns[$this->_xmlNamespace]);

        if (!$this->_xmlRoot) {
            $this->logger->error('Message Num not exists');
            return false;
        }

        if (!$this->checkImportNum()) {
            $this->logger->warning('Old message Num');
            return false;
        }

        $this->_xmlBody = $this->_xmlRoot->Body->children();

        return true;
    }

    public function checkImportNum()
    {
        $curNum    = (int) $this->_xmlRoot->Header->MessageNo;
        $dbMsgNum  = (int) \Bitrix\Main\Config\Option::get(Import::MODULE_ID, 'xml_msg_num');
        $dbMsgSent = (int) \Bitrix\Main\Config\Option::get(Import::MODULE_ID, 'xml_msg_sent');

        if ($curNum <= $dbMsgNum) {
            return false;
        }

        $this->_messageNum  = $curNum;
        $this->_receivedNum = $dbMsgSent;

        return true;
    }

    public function setAnswer()
    {
        $this->_receivedNum++;

        $data  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        $data .= "<v8msg:Message xmlns:v8msg=\"http://v8.1c.ru/messages\">\n";
        $data .= "\t<v8msg:Header>\n";
        $data .= "\t\t<v8msg:ExchangePlan>UPN_Site</v8msg:ExchangePlan>\n";
        $data .= "\t\t<v8msg:To>UPN</v8msg:To>\n";
        $data .= "\t\t<v8msg:From>Site</v8msg:From>\n";
        $data .= "\t\t<v8msg:MessageNo>" . $this->_receivedNum . "</v8msg:MessageNo>\n";
        $data .= "\t\t<v8msg:ReceivedNo>" . $this->_messageNum . "</v8msg:ReceivedNo>\n";
        $data .= "\t</v8msg:Header>\n";
        $data .= "\t<v8msg:Body></v8msg:Body>\n";
        $data .= "</v8msg:Message>\n";

        try{
            file_put_contents($this->_workDir . '/' . self::EXPORT_FILE, $data);
        } catch (Exception $e) {
            $this->logger->error("Can't save answer: ", $e->getMessage());

            return false;
        }

        \Bitrix\Main\Config\Option::set(Import::MODULE_ID, 'xml_msg_num', $this->_receivedNum);
        \Bitrix\Main\Config\Option::set(Import::MODULE_ID, 'xml_msg_sent', $this->_messageNum);

        return true;
    }

    public function processObjects($data)
    {
        if (!$data->Object) {
            $this->logger->error('no objects. skip proccess');
            return false;
        }

        $fields = [
            'IMPORT_ID' => ['name' => 'ID'],
            'NAME'      => ['name' => 'Name'],
            'CITY'      => ['name' => 'City'],
            'DISTRICT'  => ['name' => 'District'],
            'SUBWAY'    => ['name' => 'Subway', 'type' => 'array']
        ];

        foreach ($data->Object as $obj) {
            $row = $this->getRowData($fields, $obj);

            $this->_data['objects'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processBuildings($data)
    {
        if (empty($this->_data['objects'])) {
            $this->logger->warning('no objects. skip proccess');

            return false;
        }

        if (!$data->Building) {
            $this->logger->warning('no buildings. skip proccess');

            return false;
        }

        $fields = array(
            'IMPORT_ID' => ['name' => 'ID'],
            'NAME'      => ['name' => 'Name'],
            'PARENT'    => ['name' => 'Object']
        );

        foreach ($data->Building as $build) {
            $row = $this->getRowData($fields, $build);
            $objID = $row['PARENT'];
            unset($row['PARENT']);
            if (!isset($this->_data['objects'][$objID])) {
                $this->logger->error('Obj ID not exists in data', ['objID' => $objID, $row]);
                continue;
            }
            $this->_data['objects'][$objID]['buildings'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processFlats($data)
    {
        if (empty($this->_data['objects'])) {
            $this->logger->warning('no objects. skip proccess');

            return false;
        }

        if (!$data->Flat) {
            $this->logger->warning('no flats. skip proccess');

            return false;
        }

        $fields = array(
            'IMPORT_ID'    => ['name' => 'ID'],
            'NAME'         => ['name' => 'Name'],
            'FLOOR'        => ['name' => 'Floor', 'type' => 'int'],
            'AREA'         => ['name' => 'Square', 'type' => 'array'],
            'PRICE'        => ['name' => 'BasePrice', 'type' => 'float'],
            'ROOMS'        => ['name' => 'Rooms', 'type' => 'int'],
            'PLAN_TYPE'    => ['name' => 'PlanType'],
            'SECTION'      => ['name' => 'Section'],
            'STATUS'       => ['name' => 'Status', 'type' => 'int'],
            'OBJECT'       => ['name' => 'Object'],
            'BUILDING'     => ['name' => 'Building'],
            'TYPE'         => ['name' => 'Type'],
            'IMAGES'       => ['name' => 'Images', 'type' => 'array']
        );

        foreach ($data->Flat as $flat) {
            $row = $this->getRowData($fields, $flat);

            $objID = $row['OBJECT'];
            $buildID = $row['BUILDING'];
            $sectName = $row['SECTION'];
            $floorName = $row['FLOOR'];

            if (is_array($row['AREA'])) {
                $row = $this->addValuesFromArray($row, 'AREA');

                unset($row['AREA']);
            }

            if (!isset($this->_data['objects'][$objID]['buildings'][$buildID])) {
                $this->logger->error('Building ID not exists in data', ['buildID' => $buildID]);
                continue;
            }

            unset($row['OBJECT'], $row['BUILDING'], $row['SECTION'], $row['FLOOR']);

            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['NAME'] = $sectName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['NAME'] = $floorName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['flats'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processPremises($data)
    {
        if (empty($this->_data['objects'])) {
            $this->logger->warning('no objects. skip proccess');

            return false;
        }

        if (!$data->Premise) {
            $this->logger->warning('no premises. skip proccess');

            return false;
        }

        $fields = array(
            'IMPORT_ID'    => ['name' => 'ID'],
            'NAME'         => ['name' => 'Name'],
            'FLOOR'        => ['name' => 'Floor', 'type' => 'int'],
            'AREA'         => ['name' => 'Square', 'type' => 'array'],
            'PRICE'        => ['name' => 'BasePrice', 'type' => 'float'],
            'PLAN_TYPE'    => ['name' => 'PlanType'],
            'SECTION'      => ['name' => 'Section'],
            'STATUS'       => ['name' => 'Status', 'type' => 'int'],
            'OBJECT'       => ['name' => 'Object'],
            'BUILDING'     => ['name' => 'Building'],
            'TYPE'         => ['name' => 'Type'],
            'IMAGES'       => ['name' => 'Images', 'type' => 'array']
        );

        foreach($data->Premise AS $premise){
            $row = $this->getRowData($fields, $premise);

            $objID = $row['OBJECT'];
            $buildID = $row['BUILDING'];
            $sectName = $row['SECTION'];
            $floorName = $row['FLOOR'];

            if (is_array($row['AREA'])) {
                $row = $this->addValuesFromArray($row, 'AREA');

                unset($row['AREA']);
            }

            if (!isset($this->_data['objects'][$objID]['buildings'][$buildID])) {
                $this->logger->error('Building ID not exists in data', ['buildID' => $buildID]);
                continue;
            }

            unset($row['OBJECT'], $row['BUILDING'], $row['SECTION'], $row['FLOOR']);

            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['NAME'] = $sectName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['NAME'] = $floorName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['premises'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processReferenceBook($data)
    {
        // Statuses
        //
        if (empty($data->Status->Status)) {
            $this->logger->info("No Statuses. skip");
        } else {
            foreach ($data->Status->Status as $v) {
                $k = (int) $v['id'];
                $v = (string) $v;
                $this->_data['statuses'][$k] = [
                    'IMPORT_ID' => $k,
                    'NAME'      => $v
                ];
            }
        }

        // Flats Type
        //
        if (empty($data->FlatType->Type)) {
            $this->logger->info("No Flats types. skip");
        } else {
            foreach ($data->FlatType->Type as $v) {
                $k = (int) $v['id'];
                $this->_data['flatTypes'][$k] = [
                    'IMPORT_ID' => $k,
                    'NAME'      => (string) $v,
                    'ROOMS'     => (int) $v['rooms']
                ];
            }
        }

        // Premise Type
        //
        if (empty($data->PremiseType->Type)) {
            $this->logger->info("No Premise types. skip");
        } else {
            foreach ($data->PremiseType->Type as $v) {
                $k = (int) $v['id'];
                $this->_data['premiseTypes'][$k] = [
                    'IMPORT_ID' => $k,
                    'NAME'      => (string) $v
                ];
            }
        }

        return true;
    }
}