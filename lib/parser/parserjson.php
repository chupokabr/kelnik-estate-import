<?php

namespace Kelnik\EstateImport\Parser;

class ParserJSON extends ParserAbstract
{
    const IMPORT_FILE = 'data.json';

    protected $_jsonData = [];
    protected $_data = [];
    protected $_allowMethods  = [
        'ReferenceBook',
        'Objects',
        'Buildings',
        'Flats',
        'Premises'
    ];

    public function getData()
    {
        if (!$this->getJson()) {
            $this->logger->warning('no JSON data');
            return false;
        }

        $this->logger->info('parse JSON');

        foreach($this->_allowMethods as $v) {
            $methodName = 'process' . $v;
            if (!isset($this->_jsonData[$v]) || !method_exists($this, $methodName)) {
                continue;
            }

            $this->logger->info('= process "' . $v . '"');
            $this->$methodName($this->_jsonData[$v]);
            unset($this->_jsonData[$v]);
            $this->logger->info('= process "' . $v . '" complete');
        }

        $this->logger->info('parse complete');

        return $this->_data;
    }

    public function getJson()
    {
        $this->logger->info('read ' . self::IMPORT_FILE);

        $importFile = $this->_workDir . DIRECTORY_SEPARATOR . self::IMPORT_FILE;

        if (!file_exists($importFile)) {
            $this->logger->warning('File "' . self::IMPORT_FILE . '" not exists');
            return false;
        }

        try {
            $this->_jsonData = json_decode(file_get_contents($importFile), true);
        } catch (Exception $e) {
            $this->logger->error('Can\'t load JSON file: ', $e->getMessage());
            return false;
        }

        return true;
    }

    public function processObjects($data)
    {
        if (!$data) {
            $this->logger->error('no objects. skip proccess');
            return false;
        }

        $fields = [
            'IMPORT_ID' => ['name' => 'Id'],
            'NAME'      => ['name' => 'Name'],
            'CITY'      => ['name' => 'City'],
            'DISTRICT'  => ['name' => 'District'],
            'SUBWAY'    => ['name' => 'Subway', 'type' => 'array']
        ];

        foreach ($data as $obj) {
            $row = $this->getRowData($fields, $obj);

            $this->_data['objects'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processBuildings($data)
    {
        if (empty($this->_data['objects'])) {
            $this->logger->warning('no objects. skip proccess');

            return false;
        }

        if (!$data) {
            $this->logger->warning('no buildings. skip proccess');

            return false;
        }

        $fields = array(
            'IMPORT_ID' => ['name' => 'Id'],
            'NAME'      => ['name' => 'Name'],
            'PARENT'    => ['name' => 'Object']
        );

        foreach ($data as $build) {
            $row = $this->getRowData($fields, $build);
            $objID = $row['PARENT'];
            unset($row['PARENT']);
            if (!isset($this->_data['objects'][$objID])) {
                $this->logger->error('Obj ID not exists in data', ['objID' => $objID, $row]);
                continue;
            }
            $this->_data['objects'][$objID]['buildings'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processFlats($data)
    {
        if (empty($this->_data['objects'])) {
            $this->logger->warning('no objects. skip proccess');

            return false;
        }

        if (!$data) {
            $this->logger->warning('no flats. skip proccess');

            return false;
        }

        $fields = array(
            'IMPORT_ID'    => ['name' => 'Id'],
            'NAME'         => ['name' => 'Name'],
            'FLOOR'        => ['name' => 'Floor', 'type' => 'int'],
            'AREA'         => ['name' => 'Square', 'type' => 'array'],
            'PRICE'        => ['name' => 'BasePrice', 'type' => 'float'],
            'ROOMS'        => ['name' => 'Rooms', 'type' => 'int'],
            'PLAN_TYPE'    => ['name' => 'PlanType'],
            'SECTION'      => ['name' => 'Section'],
            'STATUS'       => ['name' => 'Status', 'type' => 'int'],
            'OBJECT'       => ['name' => 'Object'],
            'BUILDING'     => ['name' => 'Building'],
            'TYPE'         => ['name' => 'Type'],
            'IMAGES'       => ['name' => 'Images', 'type' => 'array']
        );

        foreach ($data as $flat) {
            $row = $this->getRowData($fields, $flat);

            $objID = $row['OBJECT'];
            $buildID = $row['BUILDING'];
            $sectName = $row['SECTION'];
            $floorName = $row['FLOOR'];

            if (is_array($row['AREA'])) {
                $row = $this->addValuesFromArray($row, 'AREA');

                unset($row['AREA']);
            }

            if (!isset($this->_data['objects'][$objID]['buildings'][$buildID])) {
                $this->logger->error('Building ID not exists in data', ['buildID' => $buildID]);
                continue;
            }

            unset($row['OBJECT'], $row['BUILDING'], $row['SECTION'], $row['FLOOR']);

            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['NAME'] = $sectName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['NAME'] = $floorName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['flats'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processPremises($data)
    {
        if (empty($this->_data['objects'])) {
            $this->logger->warning('no objects. skip proccess');

            return false;
        }

        if (!$data) {
            $this->logger->warning('no premises. skip proccess');

            return false;
        }

        $fields = array(
            'IMPORT_ID'    => ['name' => 'Id'],
            'NAME'         => ['name' => 'Name'],
            'FLOOR'        => ['name' => 'Floor', 'type' => 'int'],
            'AREA'         => ['name' => 'Square', 'type' => 'array'],
            'PRICE'        => ['name' => 'BasePrice', 'type' => 'float'],
            'PLAN_TYPE'    => ['name' => 'PlanType'],
            'SECTION'      => ['name' => 'Section'],
            'STATUS'       => ['name' => 'Status', 'type' => 'int'],
            'OBJECT'       => ['name' => 'Object'],
            'BUILDING'     => ['name' => 'Building'],
            'TYPE'         => ['name' => 'Type'],
            'IMAGES'       => ['name' => 'Images', 'type' => 'array']
        );

        foreach ($data as $premise) {
            $row = $this->getRowData($fields, $premise);

            $objID = $row['OBJECT'];
            $buildID = $row['BUILDING'];
            $sectName = $row['SECTION'];
            $floorName = $row['FLOOR'];

            if (is_array($row['AREA'])) {
                $row = $this->addValuesFromArray($row, 'AREA');

                unset($row['AREA']);
            }

            if (!isset($this->_data['objects'][$objID]['buildings'][$buildID])) {
                $this->logger->error('Building ID not exists in data', ['buildID' => $buildID]);
                continue;
            }

            unset($row['OBJECT'], $row['BUILDING'], $row['SECTION'], $row['FLOOR']);

            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['NAME'] = $sectName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['NAME'] = $floorName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['premises'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processReferenceBook($data)
    {
        // Statuses
        //
        if (empty($data['Status'])) {
            $this->logger->info("No Statuses. skip");
        } else {
            $fields = [
                'IMPORT_ID' => ['name' => 'id', 'type' => 'int'],
                'NAME'      => ['name' => 'name']
            ];
            foreach ($data['Status'] as $v) {
                $row = $this->getRowData($fields, $v);
                $this->_data['statuses'][$row['IMPORT_ID']] = $row;
            }
        }

        // Flats Type
        //
        if (empty($data['FlatType'])) {
            $this->logger->info("No Flats types. skip");
        } else {
            $fields = [
                'IMPORT_ID' => ['name' => 'id', 'type' => 'int'],
                'NAME'      => ['name' => 'name'],
                'ROOMS'     => ['name' => 'rooms', 'type' => 'int']
            ];
            foreach ($data['FlatType'] as $v) {
                $row = $this->getRowData($fields, $v);
                $this->_data['flatTypes'][$row['IMPORT_ID']] = $row;
            }
        }

        // Premise Type
        //
        if (empty($data['PremiseType'])) {
            $this->logger->info("No Premise types. skip");
        } else {
            $fields = [
                'IMPORT_ID' => ['name' => 'id', 'type' => 'int'],
                'NAME'      => ['name' => 'name']
            ];
            foreach ($data['PremiseType'] as $v) {
                $row = $this->getRowData($fields, $v);
                $this->_data['premiseTypes'][$row['IMPORT_ID']] = $row;
            }
        }

        return true;
    }
}