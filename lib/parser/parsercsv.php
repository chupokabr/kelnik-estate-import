<?php

namespace Kelnik\EstateImport\Parser;

class ParserCSV extends ParserAbstract
{
    const DELIMITER = ';';

    protected $_data = [];
    protected $_allowMethods  = [
        'Statuses',
        'FlatTypes',
        'PremiseTypes',
        'Objects',
        'Buildings',
        'Flats',
        'Premises'
    ];

    public function getData()
    {
        $this->logger->info('parse CSV');

        foreach($this->_allowMethods as $v) {
            $methodName = 'process' . $v;
            if (!method_exists($this, $methodName)) {
                continue;
            }

            $this->logger->info('= process "' . $v . '"');
            $this->$methodName();
            $this->logger->info('= process "' . $v . '" complete');
        }

        $this->logger->info('parse complete');

        return $this->_data;
    }

    public function processObjects()
    {
        if (!$data = $this->readCsvFile('objects')) {
            $this->logger->error('no objects. skip proccess');
            return false;
        }

        $fields = [
            'IMPORT_ID' => ['name' => 'Id'],
            'NAME'      => ['name' => 'Name'],
            'CITY'      => ['name' => 'City'],
            'DISTRICT'  => ['name' => 'District'],
            'SUBWAY'    => ['name' => 'Subway', 'type' => 'array']
        ];

        foreach ($data as $obj) {
            $row = $this->getRowData($fields, $obj);

            $this->_data['objects'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processBuildings($data)
    {
        if (empty($this->_data['objects'])) {
            $this->logger->warning('no objects. skip proccess');

            return false;
        }

        if (!$data = $this->readCsvFile('buildings')) {
            $this->logger->warning('no buildings. skip proccess');

            return false;
        }

        $fields = array(
            'IMPORT_ID' => ['name' => 'Id'],
            'NAME'      => ['name' => 'Name'],
            'PARENT'    => ['name' => 'Object']
        );

        foreach ($data as $build) {
            $row = $this->getRowData($fields, $build);
            $objID = $row['PARENT'];
            unset($row['PARENT']);
            if (!isset($this->_data['objects'][$objID])) {
                $this->logger->error('Obj ID not exists in data', ['objID' => $objID, $row]);
                continue;
            }
            $this->_data['objects'][$objID]['buildings'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processFlats($data)
    {
        if (empty($this->_data['objects'])) {
            $this->logger->warning('no objects. skip proccess');

            return false;
        }

        if (!$data = $this->readCsvFile('flats')) {
            $this->logger->warning('no flats. skip proccess');

            return false;
        }

        $fields = array(
            'IMPORT_ID'    => ['name' => 'Id'],
            'NAME'         => ['name' => 'Name'],
            'FLOOR'        => ['name' => 'Floor', 'type' => 'int'],
            'AREA'         => ['name' => 'Square', 'type' => 'array'],
            'PRICE'        => ['name' => 'BasePrice', 'type' => 'float'],
            'ROOMS'        => ['name' => 'Rooms', 'type' => 'int'],
            'PLAN_TYPE'    => ['name' => 'PlanType'],
            'SECTION'      => ['name' => 'Section'],
            'STATUS'       => ['name' => 'Status', 'type' => 'int'],
            'OBJECT'       => ['name' => 'Object'],
            'BUILDING'     => ['name' => 'Building'],
            'TYPE'         => ['name' => 'Type'],
            'IMAGES'       => ['name' => 'Images', 'type' => 'array']
        );

        foreach ($data as $flat) {
            $row = $this->getRowData($fields, $flat);

            $objID = $row['OBJECT'];
            $buildID = $row['BUILDING'];
            $sectName = $row['SECTION'];
            $floorName = $row['FLOOR'];

            if (is_array($row['AREA'])) {
                $row = $this->addValuesFromArray($row, 'AREA');

                unset($row['AREA']);
            }

            if (!isset($this->_data['objects'][$objID]['buildings'][$buildID])) {
                $this->logger->error('Building ID not exists in data', ['buildID' => $buildID]);
                continue;
            }

            unset($row['OBJECT'], $row['BUILDING'], $row['SECTION'], $row['FLOOR']);

            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['NAME'] = $sectName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['NAME'] = $floorName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['flats'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processPremises($data)
    {
        if (empty($this->_data['objects'])) {
            $this->logger->warning('no objects. skip proccess');

            return false;
        }

        if (!$data = $this->readCsvFile('premises')) {
            $this->logger->warning('no premises. skip proccess');

            return false;
        }

        $fields = array(
            'IMPORT_ID'    => ['name' => 'Id'],
            'NAME'         => ['name' => 'Name'],
            'FLOOR'        => ['name' => 'Floor', 'type' => 'int'],
            'AREA'         => ['name' => 'Square', 'type' => 'array'],
            'PRICE'        => ['name' => 'BasePrice', 'type' => 'float'],
            'PLAN_TYPE'    => ['name' => 'PlanType'],
            'SECTION'      => ['name' => 'Section'],
            'STATUS'       => ['name' => 'Status', 'type' => 'int'],
            'OBJECT'       => ['name' => 'Object'],
            'BUILDING'     => ['name' => 'Building'],
            'TYPE'         => ['name' => 'Type'],
            'IMAGES'       => ['name' => 'Images', 'type' => 'array']
        );

        foreach ($data as $premise) {
            $row = $this->getRowData($fields, $premise);

            $objID = $row['OBJECT'];
            $buildID = $row['BUILDING'];
            $sectName = $row['SECTION'];
            $floorName = $row['FLOOR'];

            if (is_array($row['AREA'])) {
                $row = $this->addValuesFromArray($row, 'AREA');

                unset($row['AREA']);
            }

            if (!isset($this->_data['objects'][$objID]['buildings'][$buildID])) {
                $this->logger->error('Building ID not exists in data', ['buildID' => $buildID]);
                continue;
            }

            unset($row['OBJECT'], $row['BUILDING'], $row['SECTION'], $row['FLOOR']);

            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['NAME'] = $sectName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['NAME'] = $floorName;
            $this->_data['objects'][$objID]['buildings'][$buildID]['sections'][$sectName]['floors'][$floorName]['premises'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processStatuses()
    {
        if (!$data = $this->readCsvFile('statuses')) {
            $this->logger->info("No Statuses. skip");
            return false;
        }

        $fields = [
            'IMPORT_ID' => ['name' => 'Id', 'type' => 'int'],
            'NAME'      => ['name' => 'Name']
        ];

        foreach ($data as $v) {
            $row = $this->getRowData($fields, $v);
            $this->_data['statuses'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processFlatTypes()
    {
        if (!$data = $this->readCsvFile('flat_types')) {
            $this->logger->info("No Flat types. skip");
            return false;
        }

        $fields = [
            'IMPORT_ID' => ['name' => 'Id', 'type' => 'int'],
            'NAME'      => ['name' => 'Name'],
            'ROOMS'     => ['name' => 'Rooms', 'type' => 'int']
        ];

        foreach ($data as $v) {
            $row = $this->getRowData($fields, $v);
            $this->_data['flatTypes'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    public function processPremiseTypes()
    {
        if (!$data = $this->readCsvFile('premise_types')) {
            $this->logger->info("No Premise types. skip");
            return false;
        }

        $fields = [
            'IMPORT_ID' => ['name' => 'Id', 'type' => 'int'],
            'NAME'      => ['name' => 'Name']
        ];

        foreach ($data as $v) {
            $row = $this->getRowData($fields, $v);
            $this->_data['premiseTypes'][$row['IMPORT_ID']] = $row;
        }

        return true;
    }

    // Helpers
    //
    public function readCsvFile($fileName)
    {
        if (!$fileName) {
            return false;
        }

        $filePath = $this->_workDir . DIRECTORY_SEPARATOR . $fileName . '.csv';

        if (!file_exists($filePath) || !is_readable($filePath)) {
            return false;
        }

        $data = [];

        $fp = fopen($filePath, 'r');
        $i  = 0;
        $fields = [];

        while (!feof($fp)) {
            $str = fgetcsv($fp, 4096, self::DELIMITER, '"', '\\');
            if (!$i) {
                $fields = $str;
                $i++;
                continue;
            }

            $row = [];

            foreach ($fields as $k => $field) {
                $row[$field] = $str[$k];

                if (strpos($row[$field], '|') !== false) {
                    $row[$field] = explode('|', $row[$field]);
                }
            }

            $data[] = $row;
        }
        fclose($fp);

        return $data;
    }
}