<?php

namespace Kelnik\EstateImport\Parser;

interface ParserInterface
{
    public function getData();
}