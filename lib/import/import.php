<?php

namespace Kelnik\EstateImport\Import;

use Bitrix\Estate as Estate;
use Bitrix\Main\DB\Exception;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Processor\MemoryUsageProcessor;

class Import {

    const FLAG_NAME    = '.import';
    const FLAG_EXPIRED = 21600; // 6h
    const LOG_DIR      = 'logs';
    const IMAGE_DIR    = 'images';
    const MODULE_ID    = 'kelnik.estateimport';

    protected static $_instance = false;
    protected static $_settings = [];
    protected static $_data     = [];

    public static $logger = false;

    public static $flatStatus = [
        1 => Estate\EstateFlatTable::IN_SALE_STATUS,
        2 => Estate\EstateFlatTable::NOT_IN_SALE_STATUS,
        3 => Estate\EstateFlatTable::RESERVE_STATUS
    ];

    public static $premiseStatus = [
        1 => Estate\EstatePremiseTable::IN_SALE_STATUS,
        2 => Estate\EstatePremiseTable::NOT_IN_SALE_STATUS,
        3 => Estate\EstatePremiseTable::RESERVE_STATUS,
    ];

    public static $allowFields = [
        'flat' => [
            'ID', 'IMPORT_ID', 'NAME', 'PRICE', 'ROOMS',
            'AREA_TOTAL', 'AREA_LIVING', 'AREA_KITCHEN',
            'TYPE', 'STATUS', 'PLAN_TYPE'
        ],

        'premise' => [
            'ID', 'IMPORT_ID', 'NAME', 'PRICE', 'AREA_TOTAL',
            'TYPE', 'STATUS', 'PLAN_TYPE'
        ]
    ];

    private function __construct()
    {
    }

    protected function __clone()
    {
    }

    public static function init($workDir, $parserType = 'XML')
    {
        if (!self::$_instance) {
            self::$_instance = new self();
            self::$_settings = [
                'workDir'    => $workDir,
                'logDir'     => $workDir . DIRECTORY_SEPARATOR . self::LOG_DIR,
                'imageDir'   => $workDir . DIRECTORY_SEPARATOR . self::IMAGE_DIR,
                'parserType' => mb_strtoupper($parserType),
                'logRotate'  => (int) \Bitrix\Main\Config\Option::get(self::MODULE_ID, 'log_rotate', 20)
            ];

            if (!file_exists(self::$_settings['workDir']) || !is_writeable(self::$_settings['workDir'])) {
                die('directory ' . self::$_settings['workDir'] . ' not exists or not is writeable');
            }

            if (!file_exists(self::$_settings['logDir'])) {
                mkdir(self::$_settings['logDir']);
            }

            $stream = new RotatingFileHandler(
                self::$_settings['logDir']
                . DIRECTORY_SEPARATOR . 'import' . self::$_settings['parserType'] . '.log',
                self::$_settings['logRotate']
            );
            $stream->setFormatter(
                new LineFormatter(
                    "%datetime% > %level_name% > %message% %context% %extra%\n",
                    "Y-m-d H:i:s.u"
                )
            );
            self::$logger = new Logger(self::MODULE_ID);
            self::$logger->pushHandler($stream);
            self::$logger->pushProcessor(new MemoryUsageProcessor());
        }

        return self::$_instance;
    }

    public static function execute()
    {
        self::$logger->info('=== START ===');

        if (!self::checkFlag()) {
            self::$logger->error("more then one process of import not allowed. exit");
            self::shutdown(false);

            return false;
        }

        self::setFlag();

        $parserClass = '\Kelnik\EstateImport\Parser\Parser' . self::$_settings['parserType'];
        self::$logger->info('get parser ' . $parserClass);

        if (!class_exists($parserClass)) {
            self::$logger->error('Class ' . $parserClass . ' not exists');

            return false;
        }

        $parser = new $parserClass(self::$_settings['workDir'], self::$logger);

        if (!self::$_data['importData'] = $parser->getData()) {
            self::$logger->info('empty import data');

            return false;
        }
        unset($parser);

        self::$logger->info('import data');
        self::importData();

        return true;
    }

    public static function importData()
    {
        self::getAssocByField('objects', 'EstateObjectTable');
        self::getAssocByField('buildings', 'EstateBuildingTable', ['ID', 'IMPORT_ID', 'PARENT']);
        self::getAssocByField('sections', 'EstateSectionTable', ['ID', 'IMPORT_ID', 'PARENT']);
        self::getAssocByField('floors', 'EstateFloorTable', ['ID', 'IMPORT_ID', 'PARENT']);

        if (!empty(self::$_data['importData']['statuses'])) {
            self::importStatuses(self::$_data['importData']['statuses']);
            unset(self::$_data['importData']['statuses']);
        }

        if (!empty(self::$_data['importData']['flatTypes'])) {
            self::importTypes(self::$_data['importData']['flatTypes']);
            unset(self::$_data['importData']['flatTypes']);
        }

        if (!empty(self::$_data['importData']['premiseTypes'])) {
            self::importTypes(self::$_data['importData']['premiseTypes'], 'premise');
            unset(self::$_data['importData']['premiseTypes']);
        }

        self::importDataObjects(self::$_data['importData']['objects']);
    }

    public static function importStatuses(array $data)
    {
        self::$logger->info('= Start import "status" reference =');

        if (empty(self::$_data['dbData']['statuses'])) {
            self::getAssocByField('statuses', 'EstateRefFlatStatusesTable',  ['ID', 'IMPORT_ID', 'NAME']);
        }

        foreach ($data as $v) {
            if (!empty(self::$_data['dbData']['statuses'][$v['IMPORT_ID']])) {
                $statusID  = self::$_data['dbData']['statuses'][$v['IMPORT_ID']]['ID'];
                $updData = ['NAME' => $v['NAME']];

                $res = Estate\EstateRefFlatStatusesTable::update($statusID, $updData);

                if (!$res->isSuccess()) {
                    self::$logger->error('Can\'t update Status', [$statusID, $updData, $res->getErrorMessages()]);
                }

                self::$logger->info('Update Status', [$statusID, $updData]);
                continue;
            }

            $res = Estate\EstateRefFlatStatusesTable::add($v);

            if (!$res->isSuccess()) {
                self::$logger->error('Can\'t add Status', [$v, $res->getErrorMessages()]);
                continue;
            }

            $v['ID'] = $res->getId();

            self::$_data['dbData']['statuses'][$v['IMPORT_ID']] = $v;
            self::$logger->info('Add Status', $v);
        }

        self::$logger->info('= End of import "statuses" reference =');

        return true;
    }

    public static function importTypes(array $data, $type = 'flat')
    {
        self::$logger->info('= Start import "type" reference =');

        if (!$type || !in_array($type, ['flat', 'premise'], true)) {
            self::$logger->error('param "type" is incorrect');
            self::$logger->info('= End import "type" reference =');
            return false;
        }

        $nameSpace =  '\Bitrix\Estate\EstateRef' . ($type == 'flat' ? 'Flat' : 'Premise') . 'TypesTable';

        if (!class_exists($nameSpace)) {
            self::$logger->error('namespace ' . $nameSpace . ' not found');
            self::$logger->info('= End import "type" reference =');
            return false;
        }

        if (empty(self::$_data['dbData'][$type . 'Types'])) {
            self::getAssocByField(
                $type . 'Types',
                'EstateRef' . ($type == 'flat' ? 'Flat' : 'Premise') . 'TypesTable',
                ['ID', 'IMPORT_ID', 'NAME']
            );
        }

        foreach ($data as $v) {
            if (!empty(self::$_data['dbData'][$type . 'Types'][$v['IMPORT_ID']])) {
                $typeID  = self::$_data['dbData'][$type . 'Types'][$v['IMPORT_ID']]['ID'];
                $updData = ['NAME' => $v['NAME']];

                $res = $nameSpace::update($typeID, $updData);

                if (!$res->isSuccess()) {
                    self::$logger->error('Can\'t update Type', [$nameSpace, $typeID, $updData, $res->getErrorMessages()]);
                }

                self::$logger->info('Update Type', [$nameSpace, $typeID, $updData]);
                continue;
            }

            $res = $nameSpace::add($v);

            if (!$res->isSuccess()) {
                self::$logger->error('Can\'t add Type', [$nameSpace, $v, $res->getErrorMessages()]);
                continue;
            }

            $v['ID'] = $res->getId();

            self::$_data['dbData'][$type . 'Types'][$v['IMPORT_ID']] = $v;
            self::$logger->info('Add Type', [$nameSpace, $v]);
        }

        self::$logger->info('= End of import "type" reference =');

        return true;
    }

    public static function importDataObjects(array $objects)
    {
        if (!$objects) {
            self::$logger->notice('empty objects data');
            return false;
        }

        foreach($objects as $objKey => $object) {
            $objData = [
                'NAME'      => $object['NAME'],
                'IMPORT_ID' => $objKey,
                'ACTIVE'    => 'N',
            ];

            if (!array_key_exists($objKey, self::$_data['dbData']['objects'])) {
                // add
                //
                $res = Estate\EstateObjectTable::add($objData);

                if (!$res->isSuccess()) {
                    self::$logger->error('Can\'t add Object', [$objData, $res->getErrorMessages()]);
                    continue;
                }

                $objData['ID'] = $res->getId();

                self::$logger->info('Add Object', $objData);
                self::$_data['dbData']['objects'][$objKey] = $objData;
            }

            if (!$objData['ID']) {
                $objData['ID'] = self::$_data['dbData']['objects'][$objKey]['ID'];
            }

            if (!$object['buildings']) {
                self::$logger->notice('No buildings for object', $objData);
                continue;
            }

            self::importDataBuildings($object['buildings'], $objData);
        }

        return true;
    }

    public static function importDataBuildings(array $buildings, array $objData)
    {
        foreach ($buildings as $buildKey => $building) {
            $buildData = [
                'IMPORT_ID' => $buildKey,
                'NAME'      => $building['NAME'],
                'ACTIVE'    => 'N',
                'PARENT'    => $objData['ID']
            ];

            if (!array_key_exists($buildData['IMPORT_ID'], self::$_data['dbData']['buildings'])) {
                // add
                //
                $res = Estate\EstateBuildingTable::add($buildData);

                if (!$res->isSuccess()) {
                    self::$logger->error('Can\'t add Building', [$buildData, $res->getErrorMessages()]);
                    continue;
                }

                $buildData['ID'] = $res->getId();

                self::$logger->info('Add Building', $buildData);
                self::$_data['dbData']['buildings'][$buildKey] = $buildData;
            } else {
                $buildData['ID'] = self::$_data['dbData']['buildings'][$buildKey]['ID'];
                $updData = [
                    'NAME' => $buildData['NAME'],
                    'PARENT' => $buildData['PARENT']
                ];

                $res = Estate\EstateBuildingTable::update($buildData['ID'], $updData);

                if (!$res->isSuccess()) {
                    self::$logger->error('Can\'t update Building', [$buildData['ID'], $updData, $res->getErrorMessages()]);
                    continue;
                }

                self::$logger->info('Update Building', [$buildData['ID'], $updData]);
            }

            if (!$building['sections']) {
                self::$logger->notice('No sections for building', $buildData);
                continue;
            }

            self::importDataSections(
                $building['sections'],
                [
                    'obj' => $objData,
                    'build' => $buildData
                ]
            );
        }
    }

    public static function importDataSections(array $sections, array $data)
    {
        foreach ($sections as $sectKey => $section) {

            $sectData = [
                'NAME'      => $section['NAME'],
                'ACTIVE'    => 'Y',
                'PARENT'    => $data['build']['ID'],
                'IMPORT_ID' => implode(
                                    '_',
                                    [
                                        $data['obj']['IMPORT_ID'],
                                        $data['build']['IMPORT_ID'],
                                        $sectKey
                                    ]
                                )
            ];

            if (!array_key_exists($sectData['IMPORT_ID'], self::$_data['dbData']['sections'])) {

                $res = Estate\EstateSectionTable::add($sectData);

                if (!$res->isSuccess()) {
                    self::$logger->error('Can\'t add Section', [$sectData, $res->getErrorMessages()]);
                    continue;
                }

                $sectData['ID'] = $res->getId();

                self::$logger->info('Add Section', $sectData);
                self::$_data['dbData']['sections'][$sectData['IMPORT_ID']] = $sectData;
            } else {
                $sectData['ID'] = self::$_data['dbData']['sections'][$sectData['IMPORT_ID']]['ID'];
                $updData = [
                    'NAME' => $sectData['NAME'],
                    'PARENT' => $sectData['PARENT']
                ];
                $res = Estate\EstateSectionTable::update($sectData['ID'], $updData);

                if (!$res->isSuccess()) {
                    self::$logger->error('Can\'t update Section', [$sectData['ID'], $updData, $res->getErrorMessages()]);
                    continue;
                }

                self::$logger->info('Update Section', [$sectData['ID'], $updData]);
            }

            if (!$section['floors']) {
                self::$logger->notice('No floors for section', $sectData);
                continue;
            }

            $data['sect'] = $sectData;

            self::importDataFloors(
                $section['floors'],
                $data
            );
        }
    }

    public static function importDataFloors(array $floors, array $data)
    {
        foreach ($floors as $floorKey => $floor) {
            $floorData = [
                'IMPORT_ID' => $data['sect']['IMPORT_ID'] . '_' . $floorKey,
                'PARENT' => $data['sect']['PARENT'],
                'NAME' => $floor['NAME'],
                'ACTIVE' => 'Y',
                'PARENT_SECTION' => $data['sect']['ID'],
            ];

            if (!array_key_exists($floorData['IMPORT_ID'], self::$_data['dbData']['floors'])) {
                // add
                //
                $res = Estate\EstateFloorTable::add($floorData);

                if (!$res->isSuccess()) {
                    self::$logger->error('Can\'t add Floor', [$floorData, $res->getErrorMessages()]);
                    continue;
                }

                $floorData['ID'] = $res->getId();

                self::$logger->info('Add Floor', $floorData);
                self::$_data['dbData']['floors'][$floorData['IMPORT_ID']] = $floorData;
            } else {
                $floorData['ID'] = self::$_data['dbData']['floors'][$floorData['IMPORT_ID']]['ID'];
                $updData = [
                    'NAME' => $floorData['NAME'],
                    'PARENT' => $floorData['PARENT'],
                    'PARENT_SECTION' => $floorData['PARENT_SECTION']
                ];
                $res = Estate\EstateFloorTable::update($floorData['ID'], $updData);

                if (!$res->isSuccess()) {
                    self::$logger->error('Can\'t update Floor', [$floorData['ID'], $updData, $res->getErrorMessages()]);
                    continue;
                }

                self::$logger->info('Update Floor', [$floorData['ID'], $updData]);
            }

            $data['floor'] = $floorData;

            // Flats
            //
            if (!empty($floor['flats'])) {
                foreach ($floor['flats'] as $v) {
                    self::addFlat($v, $data);
                }
            }

            // Premises
            //
            if (!empty($floor['premises'])) {
                foreach ($floor['premises'] as $v) {
                    self::addPremise($v, $data);
                }
            }
        }
    }

    public static function addFlat(array $flat, array $data)
    {
        $images = isset($flat['IMAGES']) ? $flat['IMAGES'] : [];

        $flat = self::removeFields($flat, 'flat');

        $flat['STATUS'] = self::getStatusByImportID($flat['STATUS']);
        $flat['TYPE']   = self::getTypeByImportID($flat['TYPE']);
        $flat['PARENT'] = $data['floor']['ID'];

        if (empty(self::$_data['dbData']['flats'])) {
            self::getAssocByField('flats', 'EstateFlatTable', ['ID', 'IMPORT_ID', 'PARENT', 'SECTION']);
        }

        if ($images) {
            $flat['IMAGE'] = self::getFileByName(array_shift($images));
        }

        if (!array_key_exists($flat['IMPORT_ID'], self::$_data['dbData']['flats'])) {
            $res = Estate\EstateFlatTable::add($flat);

            if (!$res->isSuccess()) {
                self::$logger->error('Can\'t add Flat', [$flat, $res->getErrorMessages()]);
                return false;
            }

            self::$logger->info('Add Flat', $flat);
            self::$_data['dbData']['flats'][$flat['IMPORT_ID']] = [
                'ID'        => $res->getId(),
                'IMPORT_ID' => $flat['IMPORT_ID'],
                'SECTION'   => $flat['SECTION'],
                'PARENT'    => $flat['PARENT']
            ];

            return true;
        }

        $flatID = self::$_data['dbData']['flats'][$flat['IMPORT_ID']]['ID'];

        if (!$flat['IMAGE']) {
            unset($flat['IMAGE']);
        }

        $res = Estate\EstateFlatTable::update($flatID, $flat);

        if (!$res->isSuccess()) {
            self::$logger->error('Can\'t update Flat', [$flat, $res->getErrorMessages()]);
            return false;
        }

        self::$logger->info('update Flat', $flat);

        return true;
    }

    public static function addPremise(array $premise, array $data)
    {
        $images = isset($flat['IMAGES']) ? $flat['IMAGES'] : [];

        $premise = self::removeFields($premise, 'premise');

        $premise['STATUS'] = self::getStatusByImportID($premise['STATUS']);
        $premise['TYPE']   = self::getTypeByImportID($premise['TYPE'], 'premise');
        $premise['PARENT'] = $data['floor']['ID'];

        if (empty(self::$_data['dbData']['premises'])) {
            self::getAssocByField('premises', 'EstatePremiseTable', ['ID', 'IMPORT_ID', 'PARENT']);
        }

        if ($images) {
            $flat['IMAGE'] = self::getFileByName(array_shift($images));
        }

        if (!array_key_exists($premise['IMPORT_ID'], self::$_data['dbData']['premises'])) {
            $res = Estate\EstatePremiseTable::add($premise);

            if (!$res->isSuccess()) {
                self::$logger->error('Can\'t add Premise', [$premise, $res->getErrorMessages()]);
                return false;
            }

            self::$logger->info('Add Premise', $premise);
            self::$_data['dbData']['premises'][$premise['IMPORT_ID']] = [
                'ID'        => $res->getId(),
                'IMPORT_ID' => $premise['IMPORT_ID'],
                'PARENT'    => $premise['PARENT']
            ];

            return true;
        }

        $premiseID = self::$_data['dbData']['premises'][$premise['IMPORT_ID']]['ID'];

        if (!$premise['IMAGE']) {
            unset($premise['IMAGE']);
        }

        $res = Estate\EstatePremiseTable::update($premiseID, $premise);

        if (!$res->isSuccess()) {
            self::$logger->error('Can\'t update Premise', [$premise,  $res->getErrorMessages()]);
            return false;
        }

        self::$logger->info('update Premise', $premise);

        return true;
    }

    public static function checkFlag()
    {
        $fPath   = self::$_settings['logDir'] . DIRECTORY_SEPARATOR . self::FLAG_NAME;
        $expired = time() - self::FLAG_EXPIRED;
        $res     = file_exists($fPath);

        if (!$res) {
            return true;
        }

        if (filemtime($fPath) < $expired) {
            self::removeFlag();
            return true;
        }

        return false;
    }

    public static function setFlag()
    {
        return file_put_contents(self::$_settings['logDir'] . DIRECTORY_SEPARATOR . self::FLAG_NAME, time());
    }

    public static function removeFlag()
    {
        unlink(self::$_settings['logDir'] . DIRECTORY_SEPARATOR . self::FLAG_NAME);
    }

    public static function shutdown($removeFlag = true)
    {
        if ($removeFlag) {
            self::removeFlag();
        }

        if (self::$logger) {
            self::$logger->info("=== END ===");
        }
        self::_resetData();
    }

    protected static function _resetData()
    {
        self::$_data = [];
        self::$logger = false;
        self::$_settings = [];
        self::$_instance = false;
    }


    // Helpers
    //

    public static function sendErrorToEmail()
    {
        $message = 'Ошибка при импорте данных' . PHP_EOL;
        $message.= 'Дата: '. date('d-m-Y H:i:s') . PHP_EOL;

        $emails = \Bitrix\Main\Config\Option::get(self::MODULE_ID, 'email_import_error');

        if (!$emails) {
            return false;
        }

        $emails = explode(',', $emails);

        foreach ($emails AS $v) {
            self::$logger->notice("send mail notify to " . trim($v));
            bxmail(
                trim($v),
                'Ошибка при импорте данных',
                $message
            );
        }

        return true;
    }

    public static function removeFields(array $row, $type)
    {
        if (!$row || !$type || !isset(self::$allowFields[$type])) {
            return $row;
        }

        foreach ($row as $k => $v) {
            if (in_array($k, self::$allowFields[$type])) {
                continue;
            }
            unset($row[$k]);
        }

        return $row;
    }

    public static function getStatusByImportID($importID)
    {
        if (empty(self::$_data['dbData']['statuses'])) {
            self::getAssocByField('statuses', 'EstateRefFlatStatusesTable',  ['ID', 'IMPORT_ID', 'NAME']);
        }

        if (
            !$importID
            || empty(self::$_data['dbData']['statuses'][$importID]['ID'])
        ) {
            return 0;
        }

        return self::$_data['dbData']['statuses'][$importID]['ID'];
    }

    public static function getTypeByImportID($importID, $type = 'flat')
    {
        $nameSpace = $type === 'premise' ? 'EstateRefPremiseTypesTable' : 'EstateRefFlatTypesTable';

        if (empty(self::$_data['dbData'][$type . 'Types'])) {
            self::getAssocByField($type .'Types', $nameSpace,  ['ID', 'IMPORT_ID', 'NAME']);
        }

        if (
            !$importID
            || empty(self::$_data['dbData'][$type . 'Types'][$importID]['ID'])
        ) {
            return 0;
        }

        return self::$_data['dbData'][$type . 'Types'][$importID]['ID'];
    }

    public static function getAssocByField($param, $namespace, array $fields = [], $byField = 'IMPORT_ID')
    {
        if (!$param || !$namespace) {
            return false;
        }

        if (!$fields) {
            $fields = ['ID', 'IMPORT_ID'];
        }

        $namespace = '\Bitrix\Estate\\' . $namespace;

        self::$_data['dbData'][$param] = $namespace::getAssoc(
            [
                'select' => $fields
            ],
            $byField
        );

        return true;
    }

    public static function getFileByName($name, $module = self::MODULE_ID)
    {
        if (!$name) {
            return 0;
        }

        if (file_exists(self::$_settings['imageDir'] . DIRECTORY_SEPARATOR . $name)) {

            $imgExists = \CFile::GetList(
                [],
                [
                    'MODULE_ID' => $module,
                    'ORIGINAL_NAME' => $name,
                ]
            );

            if (!$imgExists->SelectedRowsCount()) {
                $img = \CFile::MakeFileArray(self::$_settings['imageDir'] . DIRECTORY_SEPARATOR . $name);
                $img['MODULE_ID'] = $module;

                if ($res = \CFile::SaveFile($img, $module)) {
                    self::$logger->info('Add new file', $img);
                    return $res;
                }

                return 0;
            }

            while($row = $imgExists->GetNext()){
                return $row['ID'];
            }
        }

        return 0;
    }
}