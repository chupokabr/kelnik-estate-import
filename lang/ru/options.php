<?php

$MESS['KELNIK_IMPORT_TAB_COMMON_NAME'] = 'Основное';
$MESS['KELNIK_IMPORT_TAB_COMMON_TITLE'] = 'Общие настройки импорта';
$MESS['KELNIK_IMPORT_EMAIL_IMPORT_ERROR'] = 'E-mail для ошибок при импорте';
$MESS['KELNIK_IMPORT_LOG_ROTATE'] = 'Максимальное число файлов логирования импорта';
$MESS['KELNIK_IMPORT_LOG_ROTATE_LIMIT'] = '0 - неограничено';
$MESS['KELNIK_IMPORT_REFERENCES_OPTIONS_SAVED'] = 'Изменения сохранены';
$MESS['KELNIK_IMPORT_REFERENCES_INVALID_VALUE'] = 'Ошибка';
$MESS['KELNIK_IMPORT_TAB_XML_NAME'] = 'Импорт XML';
$MESS['KELNIK_IMPORT_TAB_XML_TITLE'] = 'Настройки иморта xml';
$MESS['KELNIK_IMPORT_XML_MSG_NUM'] = 'Номер принятого сообщения';
$MESS['KELNIK_IMPORT_XML_MSG_SENT'] = 'Номер отправленного сообщения';