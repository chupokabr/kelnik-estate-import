<?php
set_time_limit(6000);

$docRoot  = realpath(__DIR__);

if (empty($_SERVER['DOCUMENT_ROOT'])) {
    $_SERVER['DOCUMENT_ROOT'] = $docRoot;
}

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

require $docRoot . '/bitrix/modules/main/include/prolog_before.php';

ini_set('display_errors', 'On');
error_reporting(E_ALL & ~E_STRICT & ~E_DEPRECATED & ~E_NOTICE & ~E_WARNING);

if (!CModule::IncludeModule('estate')) {
    die('Estate Module not installed');
}

if (!CModule::IncludeModule('kelnik.estateimport')) {
    die('Module "Kelnik import" is not installed');
}

use Kelnik\EstateImport\Import\Import;

Import::init(implode(DIRECTORY_SEPARATOR, [$docRoot, 'upload', Import::MODULE_ID]));
Import::execute();
Import::shutdown();

require $docRoot . '/bitrix/modules/main/include/epilog_after.php';