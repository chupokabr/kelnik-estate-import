<?php

use Bitrix\Main\Application;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);

class kelnik_estateimport extends CModule
{
    public $MODULE_ID = 'kelnik.estateimport';

    function __construct()
    {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('KELNIK_ESTATE_IMPORT_COMPLETE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('KELNIK_ESTATE_IMPORT_COMPLETE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('KELNIK_ESTATE_IMPORT_PARTNER_NAME');
        $this->PARTNER_URI = 'http://www.kelnik.ru';
    }

    public function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->InstallFiles();

        $options = [
            'log_rotate' => 20,
            'xml_msg_num' => 0,
            'xml_msg_sent' => 0,
        ];

        foreach ($options as $k => $v) {
            \Bitrix\Main\Config\Option::set($this->MODULE_ID, $k, $v);
        }
    }

    public function DoUninstall()
    {
        $this->UnInstallFiles();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function InstallFiles()
    {
        return CopyDirFiles(
            $_SERVER['DOCUMENT_ROOT'] . getLocalPath('modules/' . $this->MODULE_ID . '/install/scripts/importEstate.php'),
            $_SERVER['DOCUMENT_ROOT'] . '/importEstate.php'
        );
    }

    public function UnInstallFiles()
    {
        return DeleteDirFilesEx('/importEstate.php');
    }
}